
function saveOptions(e) {
    e.preventDefault();
    browser.storage.local.set({
      waitTime: document.querySelector("#waitTime").value
    });
    browser.runtime.sendMessage({"newWaitTime": document.querySelector("#waitTime").value});
  }
  
  function restoreOptions() {
  
    function setCurrentChoice(result) {
      document.querySelector("#waitTime").value = result.waitTime || "15";
    }
  
    function onError(error) {
      console.log(`Error: ${error}`);
    }
  
    var getting = browser.storage.local.get("waitTime");
    getting.then(setCurrentChoice, onError);
  }
  
  document.addEventListener("DOMContentLoaded", restoreOptions);
  document.querySelector("form").addEventListener("submit", saveOptions);